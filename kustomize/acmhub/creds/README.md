This will create the credentials used by the hub cluster to manage clouds.
See also https://github.com/dlbewley/demo-acm/tree/main/kustomize/clusterdeployment/base
