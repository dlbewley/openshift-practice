:toc:
:icons: font
:sectanchors:
:sectlinks:
:ocp-ver: 4.12
:install-doc-title: Installing a cluster on vSphere with Agent Based Installer
:provider: baremetal
:cluster-name: agent
:cluster-basedomain: lab.bewley.net
:install-doc-url: https://docs.openshift.com/container-platform/{ocp-ver}/installing/installing_with_agent_based_installer/preparing-to-install-with-agent-based-installer.html
:install-doc-link: {install-doc-url}[{install-doc-title}]
:install-update-link: https://docs.openshift.com/container-platform/{ocp-ver}/architecture/architecture-installation.html#architecture-installation[Installation and update]
:source-highlighter: rouge
:toc[]:

= OpenShift Agent-based Baremetal Install

.Docs
* https://docs.openshift.com/container-platform/{ocp-ver}/installing/installing_with_agent_based_installer/preparing-to-install-with-agent-based-installer.html
* https://docs.openshift.com/container-platform/{ocp-ver}/installing/installing_with_agent_based_installer/installing-with-agent-based-installer.html
* https://cloud.redhat.com/blog/meet-the-new-agent-based-openshift-installer-1

== Prereqs

=== nmstatectl binary

When doing static network configuration the nmstatectl binary must be on the system running the openshift-install binary.

This will eventually be bundled into the openshift-install binary. See https://issues.redhat.com/browse/AGENT-139

For now, download from here https://github.com/nmstate/nmstate/releases

https://github.com/nmstate/nmstate/releases/download/v2.2.10/nmstatectl-macos-x64.zip


=== IP and DNS Delegation

.**Keepalived**
[TIP]
These IPs will be handled by keepalived on the nodes. No need to setup an external load balancer.

* Create subdomain `{cluster-name}` for cluster in base domain `{cluster-basedomain}`
* Allocate IP and assign DNS for `api.{cluster-name}.{cluster-basedomain}.`
[source,bash]
$ dig +short api.agent.lab.bewley.net
agent-lb-api.lab.bewley.net.
192.168.4.19

* Allocate IP and assign DNS for `*.apps.{cluster-name}.{cluster-basedomain}.`
[source,bash]
$ dig +short foo.apps.agent.lab.bewley.net
agent-lb-apps.lab.bewley.net.
192.168.4.18

** API - `api.{cluster-name}.{cluster-basedomain}` at 192.168.4.19 mapping to master nodes on port 6443
** Ingress - `*.apps.{cluster-name}.{cluster-basedomain}` at 192.168.4.18 mapping to worker nodes on ports 80, 443


== Installation

=== Obtaining the installation program

* Download the `oc` client and `openshift-install` installer using https://github.com/dlbewley/homelab/blob/master/bin/getoc[getoc] script and select version with https://github.com/dlbewley/homelab/blob/master/bin/ocver[ocver] or manually at https://mirror.openshift.com/pub/openshift-v4/x86_64/clients/

== Agent-based Installer workflow

[quote,]
One of the control plane hosts runs the Assisted Service at the start of the boot process and eventually becomes the bootstrap host. This node is called the rendezvous host (node 0). The Assisted Service ensures that all the hosts meet the requirements and triggers an OpenShift Container Platform cluster deployment. All the nodes have the Red Hat Enterprise Linux CoreOS (RHCOS) image written to the disk. The non-bootstrap nodes reboot and initiate a cluster deployment. Once the nodes are rebooted, the rendezvous host reboots and joins the cluster. The bootstrapping is complete and the cluster is deployed.

=== Templates

Define templates that will be populated by environmental variables.

.agent-config.yaml.tpl
[source,yaml]
----
include::clusters/agent/agent-config.yaml.tpl[]
----

.install-config-baremetal.yaml.tpl
[source,yaml]
----
include::clusters/agent/install-config-baremetal.yaml.tpl[]
----

=== Create Env File

.env
[source,bash]
----
include::clusters/agent/env[]
----

=== Create Install Configs

This Makefile will allow for rewriting of configs based on the values in `env` and of re-running steps predictably.

.Makefile
[source,make]
----
include::clusters/Makefile[]
----

* Create install and agent configs

[source,bash]
make config

* Create agent ISO

[source,bash]
make image

* Copy resulting agent.x86_64.iso to network attached storage

[source,bash]
make publish-image

=== Deploy the cluster

* Attach agent ISO to bm01, bm02, bm03 VM and boot all hosts.

== Example Runthrough

* 
[source,bash]
----
$ make config
cat "install-config-baremetal.yaml.tpl" | envsubst > "install-config.yaml"
cat "agent-config.yaml.tpl" | envsubst > "agent-config.yaml"

$ make image
which nmstatectl
/Users/dale/bin/nmstatectl
openshift-install agent create image
INFO The rendezvous host IP (node0 IP) is 192.168.4.201
INFO Extracting base ISO from release payload
INFO Verifying cached file
INFO Using cached Base ISO /Users/dale/Library/Caches/agent/image_cache/coreos-x86_64.iso
INFO Consuming Install Config from target directory
INFO Consuming Agent Config from target directory

$ make publish-image
rsync -e ssh --progress -avz agent.x86_64.iso dale@192.168.1.40:/mnt/Vol1/vmdata/iso/ocp/
building file list ...
1 file to consider
agent.x86_64.iso
  1213202432 100%  250.97MB/s    0:00:04 (xfer#1, to-check=0/1)

sent 69825 bytes  received 243915 bytes  20241.29 bytes/sec
total size is 1213202432  speedup is 3866.90
----


* Attach ISO to hosts as boot media

.ISO attached to "baremetal" VM
image:img/baremetal-vm-agent-node.png[]

* Power on hosts and wait

.Rendezvous node console early phase of installation
image:img/baremetal-vm-agent-node-boot.png[]

== Logging in to the cluster

[source,bash]
----
$ export KUBECONFIG=auth/kubeconfig

$ oc get clusterversion
NAME      VERSION   AVAILABLE   PROGRESSING   SINCE   STATUS
version   4.12.14   True        False         11h     Cluster version is 4.12.14

$ oc get nodes
NAME       STATUS   ROLES                         AGE   VERSION
master-1   Ready    control-plane,master,worker   11h   v1.25.8+27e744f
master-2   Ready    control-plane,master,worker   11h   v1.25.8+27e744f
master-3   Ready    control-plane,master,worker   11h   v1.25.8+27e744f

$ oc get clusteroperators
...
----
