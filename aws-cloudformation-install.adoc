:ocp-ver: 4.5
:install-doc-title: Installing a cluster on user-provisioned infrastructure in AWS by using CloudFormation templates
:provider: aws
include::{provider}-vars.inc.adoc[]
:install-doc-url: https://docs.openshift.com/container-platform/{ocp-ver}/installing/installing_aws/installing-aws-user-infra.html
:install-doc-link: {install-doc-url}[{install-doc-title}]
include::meta.adoc[]
:toc[]:

= OpenShift AWS UPI Install with CloudFormation Templates

Example run through of {install-doc-link}

.**Working Directory**
[IMPORTANT]
It is expected you are working from the root directory of this repo.

== Prereqs

https://docs.openshift.com/container-platform/{ocp-ver}/installing/installing_aws/installing-aws-account.html[Configure an AWS account to host the cluster.]

=== Required AWS infrastructure components

* Download the https://github.com/openshift/installer/tree/master/upi/aws/cloudformation[provided CloudFormation templates] to link:cloudformation/example/[cloudformation/example/]

[IMPORTANT]
You must determine and implement a method of verifying the validity of the kubelet serving certificate requests and approving them.

=== Provider DNS Delegation

.Both?
[WARNING]
A public and a private Route53 zone are needed?

https://docs.openshift.com/container-platform/{ocp-ver}/installing/installing_aws/installing-aws-account.html#installation-aws-route53_installing-aws-account[Configuring Route53]

* [x] Create subdomain in {provider-dns-link}, https://docs.aws.amazon.com/Route53/latest/DeveloperGuide/CreatingHostedZone.html[HowTo]
* [x] Create Hosted Zone: aws.tofu.farm, type=public

* [x] Delegate subdomain in DNS provider
[source,bash]
$ dig +short ns aws.tofu.farm.
ns-1574.awsdns-04.co.uk.
ns-17.awsdns-02.com.
ns-1393.awsdns-46.org.
ns-719.awsdns-25.net.
$ dig +short soa aws.foru.farm@ns-17.awsdns-02.com
ns1-etm.att.net. nomail.etm.att.net. 1 604800 3600 2419200 900

=== Provider Credentials

include::{provider}-cli.inc.adoc[]

=== Provider Limits

* [x] Verify that account https://docs.aws.amazon.com/general/latest/gr/aws_service_limits.html[AWS service quotas] offer https://docs.openshift.com/container-platform/{ocp-ver}/installing/installing_aws/installing-aws-account.html#installation-aws-limits_installing-aws-account[adequate quotas]

* [x] Verify account has https://docs.openshift.com/container-platform/{ocp-ver}/installing/installing_aws/installing-aws-account.html#installation-aws-permissions_installing-aws-account[adequate permissions]

=== Internet and Telemetry access for OpenShift Container Platform

* [x] Ensure internetgateway exists for telemetry upload.

=== Generating an SSH private key and adding it to the agent

* [x] `ssh-add ~/.ssh/id_rsa`

== Installation

=== Obtaining the installation program

* [x] Download and save pull secret

[source,bash]
$ export PULL_SECRET=/Volumes/Keybase/private/dlbewley/credz/redhat/pull-secret/dbewley@redhat.com.json

- [x] Download the installer

[source,bash]
curl -O https://mirror.openshift.com/pub/openshift-v4/clients/ocp/latest/openshift-install-mac.tar.gz
tar xvzf openshift-install-mac.tar.gz openshift-install
alias openshift-install=$(pwd)/openshift-install

- [x] https://support.apple.com/guide/mac-help/open-a-mac-app-from-an-unidentified-developer-mh40616/mac[Enable execution] of the `openshift-install` binary. Ctrl-click in finder and open to bypass OSX signature check or use `spctl`.

[source,bash]
spctl --add --label "OpenShift" openshift-install

=== Creating the installation configuration file

* [x] Generate an  https://docs.openshift.com/container-platform/{ocp-ver}/installing/installing_aws/installing-aws-user-infra.html#installation-generate-aws-user-infra-install-config_installing-aws-user-infra[install-config.yaml for customization]

[source,bash]
----
export INSTALL_DIR="$(git rev-parse --show-toplevel)/clusters/aws-cfupi"
openshift-install create install-config --dir=$INSTALL_DIR
cp -p $INSTALL_DIR/install-config.yaml{,.bak-$(date +%Y%m%d)}
# or
./bin/install-prep.sh cfupi
----

=== Generate Machine Manifests

* [x] https://docs.openshift.com/container-platform/{ocp-ver}/installing/installing_aws/installing-aws-user-infra.html#installation-user-infra-generate-k8s-manifest-ignition_installing-aws-user-infra[Generate Manifests]

[source,bash]
----
cp -p $INSTALL_DIR/install-config.yaml{,.bak-$(date +%Y%m%d)}
openshift-install create manifests --dir=$INSTALL_DIR
cp -p $INSTALL_DIR/install-config.yaml{.bak-$(date +%Y%m%d),}
----

* Disable scheduling to masters

[source,bash]
sed -i'' -e "s/mastersSchedulable: true/mastersSchedulable: false/" \
  $INSTALL_DIR/manifests/cluster-scheduler-02-config.yml

* Remove the Machine manifests 

[source,bash]
rm -f $INSTALL_DIR/openshift/99_openshift-cluster-api_master-machines-*.yaml
rm -f $INSTALL_DIR/openshift/99_openshift-cluster-api_worker-machineset-*.yaml

=== Generate Ignition Configs

* Create ignition configs

[source,bash]
openshift-install create ignition-configs  --dir=$INSTALL_DIR

* Modify install-config.yaml set worker replicas to 0.

* Optionally add proxy info to install-config.yaml

* Restore install-config.yaml

[source,bash]
cp -p $INSTALL_DIR/install-config.yaml{.bak-$(date +%Y%m%d),}

=== Generate Environment file

* Begin building environment file for populating the cloudformation templates
** https://docs.openshift.com/container-platform/{ocp-ver}/installing/installing_aws/installing-aws-user-infra.html#installation-aws-user-infra-rhcos-ami_installing-aws-user-infra[Find RHCOS AMIs for the AWS region in use] 
** Obtain the zone id from route53
** Obtain the infra ID from the installer metadata.json
** Decide on VPC CIDR, subnet length, and number of AZs

[source,bash]
----
export INSTALL_DIR="$(git rev-parse --show-toplevel)/clusters/aws-cfupi"
export ENV_FILE=$INSTALL_DIR/.env
export AvailabilityZoneCount=1
export AWS_PROFILE=na-west
export CertificateAuthorities=$(cat $INSTALL_DIR/master.ign \
  | jq -r '.ignition.security.tls.certificateAuthorities[0].source')
export CLUSTER_NAME=$(jq -r .clusterName $INSTALL_DIR/metadata.json)
export ClusterName=$CLUSTER_NAME
export HostedZoneName="aws.tofu.farm"
export HostedZoneId=$(aws route53 list-hosted-zones-by-name --dns-name=$HostedZoneName |
     jq -r '.HostedZones[0].Id' | \
     sed 's!/hostedzone/!!')
export INFRA_ID=$(jq -r .infraID $INSTALL_DIR/metadata.json)
export InfrastructureName=$INFRA_ID
export MasterInstanceType=m4.xlarge
export REGION=us-west-2
export RHCOS_AMI="ami-000d6e92357ac605c"
export RhcosAmi=$RHCOS_AMI
export SubnetBits=8
export VpcCidr=10.0.0.0/16
export WorkerInstanceType=m4.large
----

[source,bash]
----
echo '#!/bin/bash' > $ENV_FILE
for evar in \
  AvailabilityZoneCount \
  AWS_PROFILE \
  CertificateAuthorities \
  CLUSTER_NAME \
  ClusterName \
  HostedZoneId \
  HostedZoneName \
  INFRA_ID \
  InfrastructureName \
  INSTALL_DIR \
  MasterInstanceType \
  REGION \
  RHCOS_AMI \
  RhcosAmi \
  SubnetBits \
  VpcCidr \
  WorkerInstanceType \
  ; do
  echo -n "$evar "
  ansible localhost -o \
    -m lineinfile \
    -a "path=$ENV_FILE regexp='^export $evar=' line='export $evar=\"{{ lookup('env','$evar') }}\"'" 2>/dev/null
done
----

=== Create AWS VPC

https://docs.openshift.com/container-platform/{ocp-ver}/installing/installing_aws/installing-aws-user-infra.html#installation-creating-aws-vpc_installing-aws-user-infra

* Copy cloudformation templates for the cluster from link:cloudformation/example/[cloudformation/example/] to link:clusters/aws-cfupi/cloudformation/[clusters/aws-cfupi/cloudformation/]

[source,bash]
cp -p cloudformation/example/* $INSTALL_DIR/cloudformation

* Define parameters in link:clusters/aws-cfupi/cloudformation/01_params.json[01_params.json]

[source,bash]
cat cloudformation/example/01_params.json \
  | jq -s '[.[][]
  | select(.ParameterKey=="AvailabilityZoneCount").ParameterValue="'"$AvailabilityZoneCount"'"
  | select(.ParameterKey=="SubnetBits").ParameterValue="'"$SubnetBits"'"
  | select(.ParameterKey=="VpcCidr").ParameterValue="'"$VpcCidr"'"]' \
  > $INSTALL_DIR/cloudformation/01_params.json

[source,bash]
----
aws cloudformation create-stack \
    --stack-name ${INFRA_ID}-01 \
    --template-body file://${INSTALL_DIR}/cloudformation/01_vpc.yaml \
    --parameters file://${INSTALL_DIR}/cloudformation/01_params.json
----

* Wait for `CREATE_COMPLETE` and capture outputs to link:clusters/aws-cfupi/cloudformation/01_outputs.json[]

[source,bash]
watch -n 10 "aws cloudformation describe-stacks --stack-name ${INFRA_ID}-01 | jq '.Stacks[0].StackStatus'"

* Place outputs in environment and serialize to env file

include::aws-save-stack-outputs.adoc[]

Below is redundant.

[source,bash]
----
IDX="01"
aws cloudformation describe-stacks --stack-name ${INFRA_ID}-${IDX} | jq '.Stacks[0].Outputs[]' \
  > $INSTALL_DIR/cloudformation/${IDX}_outputs.json
for key in $(cat $INSTALL_DIR/cloudformation/${IDX}_outputs.json | jq -r .OutputKey); do
  export $key=$(cat $INSTALL_DIR/cloudformation/${IDX}_outputs.json \
    | jq -r "select(.OutputKey==\"$key\").OutputValue")
  echo -n "$key "
  ansible localhost -o \
    -m lineinfile \
    -a "path=$ENV_FILE regexp='^export $key=' line='export $key=\"{{ lookup('env','$key') }}\"'" 2>/dev/null
done
----

=== Creating networking and load balancing components in AWS

https://docs.openshift.com/container-platform/{ocp-ver}/installing/installing_aws/installing-aws-user-infra.html#installation-creating-aws-dns_installing-aws-user-infra


* Use above and create params file for cluster infra link:clusters/aws-cfupi/cloudformation/02_params.json[]

[source,bash]
cat cloudformation/example/02_params.json \
  | jq -s '[.[][]
  | select(.ParameterKey=="ClusterName").ParameterValue="'"$ClusterName"'"
  | select(.ParameterKey=="InfrastructureName").ParameterValue="'"$InfrastructureName"'"
  | select(.ParameterKey=="HostedZoneId").ParameterValue="'"$HostedZoneId"'"
  | select(.ParameterKey=="HostedZoneName").ParameterValue="'"$HostedZoneName"'"
  | select(.ParameterKey=="PublicSubnets").ParameterValue="'"$PublicSubnetIds"'"
  | select(.ParameterKey=="PrivateSubnets").ParameterValue="'"$PrivateSubnetIds"'"
  | select(.ParameterKey=="VpcId").ParameterValue="'"$VpcId"'"]' \
  > $INSTALL_DIR/cloudformation/02_params.json

* Create the cluster_infra stack

[source,bash]
----
aws cloudformation create-stack \
    --stack-name ${INFRA_ID}-02 \
    --template-body file://${INSTALL_DIR}/cloudformation/02_cluster_infra.yaml \
    --parameters file://${INSTALL_DIR}/cloudformation/02_params.json \
    --capabilities CAPABILITY_NAMED_IAM
----

*  Wait for `CREATE_COMPLETE` and capture outputs to link:clusters/aws-cfupi/cloudformation/02_outputs.json[]

[source,bash]
watch -n 10 "aws cloudformation describe-stacks --stack-name ${INFRA_ID}-02 | jq '.Stacks[0].StackStatus'"

* Place outputs in environment and serialize to env file

[source,bash]
----
IDX="02"
aws cloudformation describe-stacks --stack-name ${INFRA_ID}-${IDX} | jq '.Stacks[0].Outputs[]' \
  > $INSTALL_DIR/cloudformation/${IDX}_outputs.json
for key in $(cat $INSTALL_DIR/cloudformation/${IDX}_outputs.json | jq -r .OutputKey); do
  export $key=$(cat $INSTALL_DIR/cloudformation/${IDX}_outputs.json \
    | jq -r "select(.OutputKey==\"$key\").OutputValue")
  echo -n "$key "
  ansible localhost -o \
    -m lineinfile \
    -a "path=$ENV_FILE regexp='^export $key=' line='export $key=\"{{ lookup('env','$key') }}\"'" 2>/dev/null
done
----

=== Creating security group and roles in AWS

https://docs.openshift.com/container-platform/{ocp-ver}/installing/installing_aws/installing-aws-user-infra.html#installation-creating-aws-security_installing-aws-user-infra

* Create link:clusters/aws-cfupi/cloudformation/03_params.json[]

[source,bash]
cat cloudformation/example/03_params.json \
  | jq -s '[.[][]
  | select(.ParameterKey=="InfrastructureName").ParameterValue="'"$InfrastructureName"'"
  | select(.ParameterKey=="PrivateSubnets").ParameterValue="'"$PrivateSubnetIds"'"
  | select(.ParameterKey=="VpcCidr").ParameterValue="'"$VpcCidr"'"
  | select(.ParameterKey=="VpcId").ParameterValue="'"$VpcId"'"]' \
  > $INSTALL_DIR/cloudformation/03_params.json

* Create the cluster_security stack

[source,bash]
----
aws cloudformation create-stack \
    --stack-name ${INFRA_ID}-03 \
    --template-body file://$INSTALL_DIR/cloudformation/03_cluster_security.yaml \
    --parameters file://$INSTALL_DIR/cloudformation/03_params.json \
    --capabilities CAPABILITY_NAMED_IAM
----

* Wait for `CREATE_COMPLETE` and capture outputs to link:clusters/aws-cfupi/cloudformation/03_outputs.json[]

[source,bash]
watch -n 10 "aws cloudformation describe-stacks --stack-name ${INFRA_ID}-03 | jq '.Stacks[0].StackStatus'"

* Place outputs in environment and serialize to env file

[source,bash]
----
IDX="03"
aws cloudformation describe-stacks --stack-name ${INFRA_ID}-${IDX} | jq '.Stacks[0].Outputs[]' \
  > $INSTALL_DIR/cloudformation/${IDX}_outputs.json
for key in $(cat $INSTALL_DIR/cloudformation/${IDX}_outputs.json | jq -r .OutputKey); do
  export $key=$(cat $INSTALL_DIR/cloudformation/${IDX}_outputs.json \
    | jq -r "select(.OutputKey==\"$key\").OutputValue")
  echo -n "$key "
  ansible localhost -o \
    -m lineinfile \
    -a "path=$ENV_FILE regexp='^export $key=' line='export $key=\"{{ lookup('env','$key') }}\"'" 2>/dev/null
done
----

=== Creating the bootstrap node in AWS

https://docs.openshift.com/container-platform/{ocp-ver}/installing/installing_aws/installing-aws-user-infra.html#installation-creating-aws-bootstrap_installing-aws-user-infra

* Create an s3 bucket and upload link:clusters/aws-cfup/bootstrap.ign

[source,bash]
----
bucket="s3://${CLUSTER_NAME}-infra"
export BootstrapIgnitionLocation="$bucket/bootstrap.ign"
ls -l ${INSTALL_DIR}/bootstrap.ign
aws s3 mb $bucket
aws s3 cp ${INSTALL_DIR}/bootstrap.ign $BootstrapIgnitionLocation
aws s3 ls $bucket

for evar in \
  BootstrapIgnitionLocation \
  ; do
  echo -n "$evar "
  ansible localhost -o \
    -m lineinfile \
    -a "path=$ENV_FILE regexp='^export $evar=' line='export $evar=\"{{ lookup('env','$evar') }}\"'" 2>/dev/null
done
----

* Create link:clusters/aws-cfupi/cloudformation/04_params.json[]

[source,bash]
cat cloudformation/example/04_params.json \
  | jq -s '[.[][]
  | select(.ParameterKey=="BootstrapIgnitionLocation").ParameterValue="'"$BootstrapIgnitionLocation"'"
  | select(.ParameterKey=="ExternalApiTargetGroupArn").ParameterValue="'"$ExternalApiTargetGroupArn"'"
  | select(.ParameterKey=="InfrastructureName").ParameterValue="'"$InfrastructureName"'"
  | select(.ParameterKey=="InternalApiTargetGroupArn").ParameterValue="'"$InternalApiTargetGroupArn"'"
  | select(.ParameterKey=="InternalServiceTargetGroupArn").ParameterValue="'"$InternalServiceTargetGroupArn"'"
  | select(.ParameterKey=="MasterSecurityGroupId").ParameterValue="'"$MasterSecurityGroupId"'"
  | select(.ParameterKey=="PublicSubnet").ParameterValue="'"$PublicSubnetIds"'"
  | select(.ParameterKey=="RegisterNlbIpTargetsLambdaArn").ParameterValue="'"$RegisterNlbIpTargetsLambda"'"
  | select(.ParameterKey=="RhcosAmi").ParameterValue="'"$RhcosAmi"'"
  | select(.ParameterKey=="VpcId").ParameterValue="'"$VpcId"'"]' \
  > $INSTALL_DIR/cloudformation/04_params.json

* Create the bootstrap stack

[source,bash]
----
aws cloudformation create-stack \
    --stack-name ${INFRA_ID}-04 \
    --template-body file://$INSTALL_DIR/cloudformation/04_cluster_bootstrap.yaml \
    --parameters file://$INSTALL_DIR/cloudformation/04_params.json \
     --capabilities CAPABILITY_NAMED_IAM
----

* Wait for `CREATE_COMPLETE` and capture outputs to link:clusters/aws-cfupi/cloudformation/04_outputs.json[]

[source,bash]
watch -n 10 "aws cloudformation describe-stacks --stack-name ${INFRA_ID}-04 | jq '.Stacks[0].StackStatus'""

[source,bash]
----
IDX="04"
aws cloudformation describe-stacks --stack-name ${INFRA_ID}-${IDX} | jq '.Stacks[0].Outputs[]' \
  > $INSTALL_DIR/cloudformation/${IDX}_outputs.json
for key in $(cat $INSTALL_DIR/cloudformation/${IDX}_outputs.json | jq -r .OutputKey); do
  export $key=$(cat $INSTALL_DIR/cloudformation/${IDX}_outputs.json \
    | jq -r "select(.OutputKey==\"$key\").OutputValue")
  echo -n "$key "
  ansible localhost -o \
    -m lineinfile \
    -a "path=$ENV_FILE regexp='^export $key=' line='export $key=\"{{ lookup('env','$key') }}\"'" 2>/dev/null
done
----

* Confirm the bootstrap node booted OK. If not check the logs

[source,bash]
----
export BOOTSTRAP_INSTANCE_ID=$(cat $INSTALL_DIR/cloudformation/04_outputs.json \
  | jq -r 'select(.OutputKey=="BootstrapInstanceId").OutputValue')
export BOOTSTRAP_IP=$(cat $INSTALL_DIR/cloudformation/04_outputs.json \
  | jq -r 'select(.OutputKey=="BootstrapPublicIp").OutputValue')

ssh core@$BOOTSTRAP_IP uptime
aws ec2 get-console-output --instance-id $BOOTSTRAP_INSTANCE_ID \
  | jq -r '.Output' > $INSTALL_DIR/bootstrap.log
----

=== Creating the control plane machines in AWS

https://docs.openshift.com/container-platform/{ocp-ver}/installing/installing_aws/installing-aws-user-infra.html#installation-creating-aws-control-plane_installing-aws-user-infra

* Create link:clusters/aws-cfupi/cloudformation/05_params.json[]
** Ensure `CertificateAuthorities` is populated from `master.ign`

[source,bash]
cat cloudformation/example/05_params.json \
  | jq -s '[.[][]
  | select(.ParameterKey=="CertificateAuthorities").ParameterValue="'"$CertificateAuthorities"'"
  | select(.ParameterKey=="ExternalApiTargetGroupArn").ParameterValue="'"$ExternalApiTargetGroupArn"'"
  | select(.ParameterKey=="PrivateHostedZoneId").ParameterValue="'"$HostedZoneId"'"
  | select(.ParameterKey=="PrivateHostedZoneName").ParameterValue="'"$HostedZoneName"'"
  | select(.ParameterKey=="IgnitionLocation").ParameterValue="'"https://api-int.${ClusterName}.${HostedZoneName}:22623/config/master"'"
  | select(.ParameterKey=="InfrastructureName").ParameterValue="'"$InfrastructureName"'"
  | select(.ParameterKey=="InternalApiTargetGroupArn").ParameterValue="'"$InternalApiTargetGroupArn"'"
  | select(.ParameterKey=="InternalServiceTargetGroupArn").ParameterValue="'"$InternalServiceTargetGroupArn"'"
  | select(.ParameterKey=="Master0Subnet").ParameterValue="'"$PrivateSubnetIds"'"
  | select(.ParameterKey=="Master1Subnet").ParameterValue="'"$PrivateSubnetIds"'"
  | select(.ParameterKey=="Master2Subnet").ParameterValue="'"$PrivateSubnetIds"'"
  | select(.ParameterKey=="MasterInstanceProfileName").ParameterValue="'"$MasterInstanceProfile"'"
  | select(.ParameterKey=="MasterInstanceType").ParameterValue="'"$MasterInstanceType"'"
  | select(.ParameterKey=="MasterSecurityGroupId").ParameterValue="'"$MasterSecurityGroupId"'"
  | select(.ParameterKey=="RegisterNlbIpTargetsLambdaArn").ParameterValue="'"$RegisterNlbIpTargetsLambda"'"
  | select(.ParameterKey=="RhcosAmi").ParameterValue="'"$RhcosAmi"'"]' \
  > $INSTALL_DIR/cloudformation/05_params.json

* Create the control plane stack

[source,bash]
----
aws cloudformation create-stack \
    --stack-name ${INFRA_ID}-05 \
    --template-body file://$INSTALL_DIR/cloudformation/05_cluster_master_nodes.yaml \
    --parameters file://$INSTALL_DIR/cloudformation/05_params.json \
     --capabilities CAPABILITY_NAMED_IAM
----

* Wait for `CREATE_COMPLETE` and capture outputs to link:clusters/aws-cfupi/cloudformation/05_outputs.json[]

[source,bash]
watch -n 10 "aws cloudformation describe-stacks --stack-name ${INFRA_ID}-05 | jq '.Stacks[0].StackStatus'"
aws cloudformation describe-stacks --stack-name ${INFRA_ID}-05 | jq '.Stacks[0].Outputs[]' \
  > $INSTALL_DIR/cloudformation/05_outputs.json

* Don't bother serializing this output

== Deploy the Cluster

=== Initializing the bootstrap node on AWS with user-provisioned infrastructure
https://docs.openshift.com/container-platform/{ocp-ver}/installing/installing_aws/installing-aws-user-infra.html#installation-aws-user-infra-bootstrap_installing-aws-user-infra

* Deploy the control plane

[source,bash]
----
mkdir -p cast
asciinema rec -i 2 cast/${INFRA_ID}-install-$(date +%Y%m%d-%H%M).cast
ssh-add -K ~/.ssh/id_rsa
export CLUSTER_DIR=$(git rev-parse --show-toplevel)/clusters/aws-cfupi
time openshift-install wait-for bootstrap-complete --dir="$CLUSTER_DIR" --log-level=debug

DEBUG OpenShift Installer 4.5.4
DEBUG Built from commit 01f5643a02f154246fab0923f8828aa9ae3b76fb
INFO Waiting up to 20m0s for the Kubernetes API at https://api.cfupi.aws.tofu.farm:6443...

----

* Check status from another window while deploy is happenning

[source,bash]
----
$ kubectl --kubeconfig=$INSTALL_DIR/auth/kubeconfig get nodes
NAME                                       STATUS     ROLES    AGE     VERSION
ip-10-0-3-111.us-west-2.compute.internal   Ready      master   6m55s   v1.18.3+012b3ec
ip-10-0-3-180.us-west-2.compute.internal   NotReady   master   14m     v1.18.3+012b3ec
ip-10-0-3-211.us-west-2.compute.internal   Ready      master   6m53s   v1.18.3+012b3ec
ip-10-0-3-218.us-west-2.compute.internal   Ready      master   6m55s   v1.18.3+012b3ec
$ ssh core@$BOOTSTRAP_IP
[core@ip-10-0-0-157 ~]$ sudo crictl ps
CONTAINER           IMAGE                                                                                                                    CREATED             STATE               NAME                    ATTEMPT             POD ID
599b9dd2e4af8       4b76762767cf0ae7a4af121c8772cf3f68e7d9e0e9ff9a87ed53caa1bbd07ca8                                                         20 minutes ago      Running             kube-apiserver          0                   e302ec32cd11b
e8c0b44273058       63ace5dc9f52f995d7f6a5a9fce0f275ae3136782af9556a9e4efeb9cc6d59e0                                                         20 minutes ago      Running             etcd-metrics            0                   a753ed95b9ac4
fd95fa320b447       quay.io/openshift-release-dev/ocp-v4.0-art-dev@sha256:f7e17957cd2e2f8e932339a9fbf234215869b4093deab87ecbeb3eaa5ae5640e   20 minutes ago      Running             etcd-member             0                   a753ed95b9ac4
1da3f3a0b8e0d       5beb68ef5ac943f221f82ad06a85397df48b369634bbc323caf458e9a7fcdcf8                                                         20 minutes ago      Running             machine-config-server   0                   06d2e91d2fda0

$ kubectl --kubeconfig=clusters/aws-cfupi/auth/kubeconfig get clusteroperators
# snip
----

=== Copy Cluster Credentials

[source,bash]
$ cp -rp /Volumes/Keybase/private/dlbewley/credz/ocp/{skel,aws-cfupi}
$ cp -rp $INSTALL_DIR/auth/* /Volumes/Keybase/private/dlbewley/credz/ocp/aws-cfupi
$ source <(ocp aws-cfupi)

== Deploy Worker Nodes

https://docs.openshift.com/container-platform/{ocp-ver}/installing/installing_aws/installing-aws-user-infra.html#installation-creating-aws-worker_installing-aws-user-infra

* Create link:clusters/aws-cfupi/cloudformation/06_params.json[]
** Ensure `CertificateAuthorities` is populated from `master.ign`


[source,bash]
cat cloudformation/example/06_params.json \
  | jq -s '[.[][]
  | select(.ParameterKey=="CertificateAuthorities").ParameterValue="'"$CertificateAuthorities"'"
  | select(.ParameterKey=="IgnitionLocation").ParameterValue="'"https://api-int.${ClusterName}.${HostedZoneName}:22623/config/worker"'"
  | select(.ParameterKey=="InfrastructureName").ParameterValue="'"$InfrastructureName"'"
  | select(.ParameterKey=="Subnet").ParameterValue="'"$PrivateSubnetIds"'"
  | select(.ParameterKey=="WorkerInstanceProfileName").ParameterValue="'"$WorkerInstanceProfile"'"
  | select(.ParameterKey=="WorkerInstanceType").ParameterValue="'"$WorkerInstanceType"'"
  | select(.ParameterKey=="RhcosAmi").ParameterValue="'"$RhcosAmi"'"
  | select(.ParameterKey=="WorkerSecurityGroupId").ParameterValue="'"$WorkerSecurityGroupId"'"]' \
  > $INSTALL_DIR/cloudformation/06_params.json


* Create the control plane stack

[source,bash]
----
aws cloudformation create-stack \
    --stack-name ${INFRA_ID}-06 \
    --template-body file://$INSTALL_DIR/cloudformation/06_cluster_worker_node.yaml \
    --parameters file://$INSTALL_DIR/cloudformation/06_params.json \
     --capabilities CAPABILITY_NAMED_IAM
----

* Wait for `CREATE_COMPLETE` and capture outputs to link:clusters/aws-cfupi/cloudformation/06_outputs.json[]

[source,bash]
watch -n 10 "aws cloudformation describe-stacks --stack-name ${INFRA_ID}-06 | jq '.Stacks[0].StackStatus'"
CREATE_COMPLETE
aws cloudformation describe-stacks --stack-name ${INFRA_ID}-06 | jq '.Stacks[0].Outputs[]' > $INSTALL_DIR/cloudformation/06_outputs.json

=== Approve Pending Certificate Signing Requests

* Repeat until no CSR are pending and worker node is up

[source,bash]
oc get csr -o go-template='{{range .items}}{{if not .status}}{{.metadata.name}}{{"\n"}}{{end}}{{end}}' | xargs oc adm certificate approve

[source,bash]
$ oc get csr
NAME        AGE     SIGNERNAME                                    REQUESTOR                                                                   CONDITION
csr-64vmw   2m39s   kubernetes.io/kube-apiserver-client-kubelet   system:serviceaccount:openshift-machine-config-operator:node-bootstrapper   Pending
csr-68xdw   17m     kubernetes.io/kube-apiserver-client-kubelet   system:serviceaccount:openshift-machine-config-operator:node-bootstrapper   Pending
csr-mhbjv   47m     kubernetes.io/kube-apiserver-client-kubelet   system:serviceaccount:openshift-machine-config-operator:node-bootstrapper   Pending
csr-tpg7h   32m     kubernetes.io/kube-apiserver-client-kubelet   system:serviceaccount:openshift-machine-config-operator:node-bootstrapper   Pending
$ oc get csr -o go-template='{{range .items}}{{if not .status}}{{.metadata.name}}{{"\n"}}{{end}}{{end}}' | xargs oc adm certificate approve
certificatesigningrequest.certificates.k8s.io/csr-64vmw approved
certificatesigningrequest.certificates.k8s.io/csr-68xdw approved
certificatesigningrequest.certificates.k8s.io/csr-mhbjv approved
certificatesigningrequest.certificates.k8s.io/csr-tpg7h approved
$ oc get csr
NAME        AGE   SIGNERNAME                                    REQUESTOR                                                                   CONDITION
csr-5pxdj   3s    kubernetes.io/kubelet-serving                 system:node:ip-10-0-3-180.us-west-2.compute.internal                        Pending
csr-64vmw   4m    kubernetes.io/kube-apiserver-client-kubelet   system:serviceaccount:openshift-machine-config-operator:node-bootstrapper   Approved,Issued
csr-68xdw   19m   kubernetes.io/kube-apiserver-client-kubelet   system:serviceaccount:openshift-machine-config-operator:node-bootstrapper   Approved,Issued
csr-mhbjv   49m   kubernetes.io/kube-apiserver-client-kubelet   system:serviceaccount:openshift-machine-config-operator:node-bootstrapper   Approved,Issued
csr-tpg7h   34m   kubernetes.io/kube-apiserver-client-kubelet   system:serviceaccount:openshift-machine-config-operator:node-bootstrapper   Approved,Issued
$ oc get csr -o go-template='{{range .items}}{{if not .status}}{{.metadata.name}}{{"\n"}}{{end}}{{end}}' | xargs oc adm certificate approve
certificatesigningrequest.certificates.k8s.io/csr-5pxdj approved
$ oc get nodes
NAME                                       STATUS     ROLES           AGE   VERSION
ip-10-0-3-12.us-west-2.compute.internal    Ready      master,worker   83m   v1.18.3+012b3ec
ip-10-0-3-180.us-west-2.compute.internal   NotReady   worker          43s   v1.18.3+012b3ec
ip-10-0-3-204.us-west-2.compute.internal   Ready      master,worker   83m   v1.18.3+012b3ec
ip-10-0-3-24.us-west-2.compute.internal    Ready      master,worker   83m   v1.18.3+012b3ec
$ oc get csr
NAME        AGE     SIGNERNAME                                    REQUESTOR                                                                   CONDITION
csr-5pxdj   51s     kubernetes.io/kubelet-serving                 system:node:ip-10-0-3-180.us-west-2.compute.internal                        Approved,Issued
csr-64vmw   4m48s   kubernetes.io/kube-apiserver-client-kubelet   system:serviceaccount:openshift-machine-config-operator:node-bootstrapper   Approved,Issued
csr-68xdw   19m     kubernetes.io/kube-apiserver-client-kubelet   system:serviceaccount:openshift-machine-config-operator:node-bootstrapper   Approved,Issued
csr-mhbjv   50m     kubernetes.io/kube-apiserver-client-kubelet   system:serviceaccount:openshift-machine-config-operator:node-bootstrapper   Approved,Issued
hhd
$ oc get nodes
NAME                                       STATUS   ROLES           AGE     VERSION
ip-10-0-3-12.us-west-2.compute.internal    Ready    master,worker   85m     v1.18.3+012b3ec
ip-10-0-3-180.us-west-2.compute.internal   Ready    worker          2m24s   v1.18.3+012b3ec
ip-10-0-3-204.us-west-2.compute.internal   Ready    master,worker   85m     v1.18.3+012b3ec
ip-10-0-3-24.us-west-2.compute.internal    Ready    master,worker   85m     v1.18.3+012b3ec

== Troubleshooting

include::install-troubleshooting.adoc[leveloffset=+1]

== Uninstallation
=== Destroy the Cluster

- [ ] https://docs.openshift.com/container-platform/{ocp-ver}/installing/installing_aws/uninstalling-cluster-aws.html[Destroy the cluster] when you are done.

[source,bash]
----
openshift-install destroy cluster --dir=$INSTALL_DIR --log-level=debug
rm $INSTALL_DIR/*.ign
aws cloudformation delete-stack --stack-name ${INFRA_ID}-06
aws cloudformation delete-stack --stack-name ${INFRA_ID}-05
aws cloudformation delete-stack --stack-name ${INFRA_ID}-04
aws cloudformation delete-stack --stack-name ${INFRA_ID}-03
aws cloudformation delete-stack --stack-name ${INFRA_ID}-02
aws cloudformation delete-stack --stack-name ${INFRA_ID}-01

#!/bin/bash
for IDX in $(seq -f %02g 6 1); do
  echo "Deleting stack ${INFRA_ID}-$IDX"
  aws cloudformation delete-stack --stack-name ${INFRA_ID}-$IDX
  sleep 1
  stack_status=$(aws cloudformation describe-stacks --stack-name ${INFRA_ID}-$IDX | jq '.Stacks[0].StackStatus')
  echo " $stack_status"
  while [[ "$stack_status" == "DELETE_IN_PROGRESS" ]]; do
    echo sleep 15
    stack_status=$(aws cloudformation describe-stacks --stack-name ${INFRA_ID}-$IDX | jq '.Stacks[0].StackStatus')
    echo " $stack_status"
  done
done
----