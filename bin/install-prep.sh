
#!/bin/bash
# Tool for establishing configuration environment sufficient to drive
# OpenShift cluster installation and future revision control of same.

REPO_ROOT="$(git rev-parse --show-toplevel)"
. $REPO_ROOT/bin/install-common.sh

verify_cluster_name
verify_config_dir

echo "Initializing an installation configuration for '$ENVIRONMENT' environment on '$PROVIDER' infrastructure provider."
echo "Storing installation configs in '$INSTALLER_CONFIG_BASE'."

verify_provider

echo ""
echo "Press enter to continue:"
read

# interactive command to initialize an installation configuration
"$INSTALLER_BINARY" create install-config --log-level=debug --dir="$INSTALLER_CONFIG_BASE"
echo ""
echo "Press enter to backup install-config.yaml and generate manifests:"
read

cp -p ${INSTALLER_CONFIG_BASE}/install-config.yaml{,.bak-$(date +%Y%m%d)}
# This will rm the install-config.yaml prematurely
"$INSTALLER_BINARY" create manifests --dir="$INSTALLER_CONFIG_BASE"
# restore install-config
cp -p ${INSTALLER_CONFIG_BASE}/install-config.yaml{.bak-$(date +%Y%m%d),}

#cp -p ${INSTALLER_CONFIG_BASE}/install-config.yaml{,.bak-$(date +%Y%m%d)}
#"$INSTALLER_BINARY" create ignition-configs --dir="$INSTALLER_CONFIG_BASE"
#cp -p ${INSTALLER_CONFIG_BASE}/install-config.yaml{.bak-$(date +%Y%m%d),}
#echo "Ignition configs dumped to '${INSTALLER_CONFIG_BASE}/*.ign'"
