#!/bin/bash

# Tool to download and extract latest nightly build for a future version.

# nightly https://mirror.openshift.com/pub/openshift-v4/clients/ocp-dev-preview/
# prod https://mirror.openshift.com/pub/openshift-v4/clients/ocp/latest/openshift-install-mac.tar.gz

VER=${1:-4.5}
BASE_URL=https://mirror.openshift.com/pub/openshift-v4/clients/ocp-dev-preview/
PLATFORM=mac
REPO_ROOT="$(git rev-parse --show-toplevel)"
DOWNLOAD_DIR="$REPO_ROOT/installer"

# Example: 4.3.0-0.nightly-2019-12-07-225115
REL=$(curl -s ${BASE_URL}/latest-${VER}/release.txt | grep Name | sed 's/^Name\:[[:space:]]*//g')

echo Downloading $REL

for plat in mac linux; do
    mkdir -p $DOWNLOAD_DIR/$plat
    pushd $DOWNLOAD_DIR/$plat
    if [ ! -f openshift-client-${plat}-${REL}.tar.gz ]; then
        curl -O ${BASE_URL}/latest-${VER}/openshift-client-${plat}-${REL}.tar.gz
        curl -O ${BASE_URL}/latest-${VER}/openshift-install-${plat}-${REL}.tar.gz
        tar xvzf openshift-install-${plat}-${REL}.tar.gz
    fi
    popd
done