
#!/bin/bash
# Tool for gathering installer logs from nodes

REPO_ROOT="$(git rev-parse --show-toplevel)"
. $REPO_ROOT/bin/install-common.sh

verify_config_dir

echo ""
echo "Hard coding of IP addresses follows. Take your grain of salt."
echo "Press enter to continue:"
read

# interactive command to initialize an installation configuration
"$INSTALLER_BINARY" gather bootstrap --dir="$INSTALLER_CONFIG_BASE" \
    --bootstrap 10.0.0.19 \
    --master 10.0.0.16 \
    --master 10.0.0.18 \
    --master 10.0.0.21
