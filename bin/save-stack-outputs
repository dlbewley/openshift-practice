#!/bin/bash
# Save cloudformation stack outputs to json and a file for sourcing to the environment.
# Currently assumes some hardcoded conventions.
#  * Cloudformation stack must be named "${INFRA_ID}-${IDX}"
#  * IDX is a 2 digit integer and must be passed on the command line
#  * Output is to $INSTALL_DIR/cloudformation/${IDX}_outputs.json and $INSTALL_DIR/.env
# Ansible is required.

IDX=$(printf %02d $1)

if [ -z "$INFRA_ID" -o -z "$INSTALL_DIR" -o -z "$IDX" ]; then
    echo "Could not validate \$INFRA_ID and \$INSTALL_DIR"
    exit 1
fi
ENV_FILE=$INSTALL_DIR/.env

aws cloudformation describe-stacks --stack-name ${INFRA_ID}-${IDX} | jq '.Stacks[0].Outputs[]' \
  > $INSTALL_DIR/cloudformation/${IDX}_outputs.json
for key in $(cat $INSTALL_DIR/cloudformation/${IDX}_outputs.json | jq -r .OutputKey); do
  export $key=$(cat $INSTALL_DIR/cloudformation/${IDX}_outputs.json \
    | jq -r "select(.OutputKey==\"$key\").OutputValue")
  echo -n "$key "
  ansible localhost -o \
    -m lineinfile \
    -a "path=$ENV_FILE regexp='^export $key=' line='export $key=\"{{ lookup('env','$key') }}\"'" 2>/dev/null
done
