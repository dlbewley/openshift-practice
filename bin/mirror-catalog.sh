#!/bin/bash -x
# example script to mirror all RH catalogsources locally
# example only!
#
# login to upstream repos
#podman login registry.redhat.io
#podman login registry.connect.redhat.com
#podman login docker.io

local_registry=quay-quay-quay.apps.vipi.lab.bewley.net
local_namespace=lab
log_file=oc-adm-catalog-mirror.log

INDEX_REG=registry.redhat.io/redhat
INDEX_TAG="v4.7"
INDEX_IMAGES="certified-operator-index \
          community-operator-index \
          redhat-marketplace-index \
          redhat-operator-index"
INDEX_IMAGES="redhat-operator-index"

REGISTRIES="registry.redhat.io \
  registry.connect.redhat.com \
  $local_registry \
  docker.io"

export REGISTRY_AUTH_FILE=$PULL_SECRET

for reg in $REGISTRIES; do
        echo "Logging into registry $reg"
        podman login "$reg"
done

for catalog_image in $INDEX_IMAGES; do
    echo "# Begin oc adm catalog mirror ${INDEX_REG}/${catalog_image}:${INDEX_TAG}" >> $log_file
#      ${INDEX_REG}/${local_namespace}/${catalog_image}:${INDEX_TAG} \
    oc adm catalog mirror \
    --insecure=true \
      ${INDEX_REG}/${catalog_image}:${INDEX_TAG} \
      $local_registry \
      -a ${REG_CREDS} \
      | tee -a $log_file
#        --dry-run \
    echo "# End $catalog_image" >> $log_file
    echo " todo create catalogsource and imagecontentsourcepolicy resources"
done