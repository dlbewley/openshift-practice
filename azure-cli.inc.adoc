
.**Azure Command Line Interface Configuration**
[TIP]
====
Start with https://portal.azure.com/#blade/Microsoft_Azure_Resources/QuickstartPlaybookBlade/guideId/intro-azure-setup[Azure Setup Guide]

Install https://docs.microsoft.com/en-us/cli/azure/install-azure-cli-macos[Azure CLI on MacOS] and https://docs.microsoft.com/en-us/cli/azure/get-started-with-azure-cli[Get Started].

**https://docs.microsoft.com/en-us/cli/azure/[Azure CLI Docs]**

[source,bash]
----
$ brew install azure-cli

$ az login
$ az group create -l westus -n ocp
$ az configure --defaults group=ocp

$ cat ~/.azure/config
[cloud]
name = AzureCloud
[defaults]
group = ocp

$ cat ~/.azure/clouds.config
[AzureCloud]
subscription = 00000000-0000-0000-0000-000000000000

----
====