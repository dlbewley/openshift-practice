---
# rules for use with Agent installer
# - baremetal, vsphere, and none platforms are supported.
# - If `none` is used, the number of control plane replicas must be 1 and the total number of worker replicas must be 0.
# - apiVIPs and ingressVIPs parameters must be set for bare metal and vSphere platforms and not for `none`.
apiVersion: v1
metadata:
  name: ${CLUSTER_NAME}

baseDomain: ${BASEDOMAIN}

controlPlane:
  architecture: amd64
  hyperthreading: Enabled
  name: master
  replicas: ${REPLICAS_CONTROL}
  platform: {}

compute:
  - architecture: amd64
    hyperthreading: Enabled
    name: worker
    replicas: ${REPLICAS_WORKER} 
    platform: {}

networking:
  networkType: OVNKubernetes
  clusterNetwork:
    - cidr: ${CIDR_CLUSTER}
      hostPrefix: 23
  machineNetwork:
    - cidr: ${CIDR_MACHINE}
  serviceNetwork:
    - ${CIDR_SERVICES}

platform:
  baremetal:
    apiVIPs:
      - ${VIP_API}
    ingressVIPs:
      - ${VIP_INGRESS}

# For disconnected installation with pullthrough registry proxy
# imageContentSources:
#   - mirrors:
#     - ${LOCAL_REGISTRY}:${LOCAL_REGISTRY_PORT}/openshift-release-dev/ocp-release
#     source: quay.io/openshift-release-dev/ocp-release
#   - mirrors:
#     - ${LOCAL_REGISTRY}:${LOCAL_REGISTRY_PORT}/openshift-release-dev/ocp-v4.0-art-dev
#     source: quay.io/openshift-release-dev/ocp-v4.0-art-dev

publish: External
pullSecret: '${PULL_SECRET}'
sshKey: |
  ${SSH_KEY}
additionalTrustBundle: |
$TRUST_BUNDLE
