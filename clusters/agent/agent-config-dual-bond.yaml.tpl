# default interface is eno1
# bond eno2 and eno3
# next step after testing will be to 802.1q on top of bond
---
apiVersion: v1alpha1
kind: AgentConfig
metadata:
  name: ${CLUSTER_NAME}
rendezvousIP: ${RENDEZVOUS_IP}
hosts:
  - hostname: ${BM01_HOSTNAME}
    interfaces:
      - name: eno1
        macAddress: ${BM01_MAC}
      - name: eno2
        macAddress: ${BM01_MAC_2}
      - name: eno3
        macAddress: ${BM01_MAC_3}
      - name: eno4
        macAddress: ${BM01_MAC_4}
    networkConfig:
      interfaces:
        - name: bond0
          type: bond
          state: up
          ipv4:
            enabled: true
            address:
              - ip: ${BM01_IP}
                prefix-length: 24          
            dhcp: false
          link-aggregation:
            mode: active-backup 
            options:
              miimon: "150" 
            port:
              - eno1
              - eno2       

        - name: bond1
          type: bond
          state: up
          ipv4:
            enabled: false
          link-aggregation:
            mode: active-backup 
            options:
              miimon: "150" 
            port:
              - eno3
              - eno4
      dns-resolver:
        config:
          server:
            - ${NAMESERVER}
      routes:
        config:
          - destination: 0.0.0.0/0
            next-hop-address: ${GATEWAY}
            next-hop-interface: bond0
            table-id: 254

  - hostname: ${BM02_HOSTNAME}
    interfaces:
      - name: eno1
        macAddress: ${BM02_MAC}
      - name: eno2
        macAddress: ${BM02_MAC_2}
      - name: eno3
        macAddress: ${BM02_MAC_3}
      - name: eno4
        macAddress: ${BM02_MAC_4}
    networkConfig:
      interfaces:
        - name: bond0
          type: bond
          state: up
          ipv4:
            enabled: true
            address:
              - ip: ${BM02_IP}
                prefix-length: 24          
            dhcp: false
          link-aggregation:
            mode: active-backup 
            options:
              miimon: "150" 
            port:
              - eno1
              - eno2       

        - name: bond1
          type: bond
          state: up
          ipv4:
            enabled: false
          link-aggregation:
            mode: active-backup 
            options:
              miimon: "150" 
            port:
              - eno3
              - eno4
      dns-resolver:
        config:
          server:
            - ${NAMESERVER}
      routes:
        config:
          - destination: 0.0.0.0/0
            next-hop-address: ${GATEWAY}
            next-hop-interface: bond0
            table-id: 254   

  - hostname: ${BM03_HOSTNAME}
    interfaces:
      - name: eno1
        macAddress: ${BM03_MAC}
      - name: eno2
        macAddress: ${BM03_MAC_2}
      - name: eno3
        macAddress: ${BM03_MAC_3}
      - name: eno4
        macAddress: ${BM03_MAC_4}
    networkConfig:
      interfaces:
        - name: bond0
          type: bond
          state: up
          ipv4:
            enabled: true
            address:
              - ip: ${BM03_IP}
                prefix-length: 24          
            dhcp: false
          link-aggregation:
            mode: active-backup 
            options:
              miimon: "150" 
            port:
              - eno1
              - eno2          

        - name: bond1
          type: bond
          state: up
          ipv4:
            enabled: false
          link-aggregation:
            mode: active-backup 
            options:
              miimon: "150" 
            port:
              - eno3
              - eno4
      dns-resolver:
        config:
          server:
            - ${NAMESERVER}
      routes:
        config:
          - destination: 0.0.0.0/0
            next-hop-address: ${GATEWAY}
            next-hop-interface: bond0
            table-id: 254   