# bond the primary interface.
# this was only partially successful with ovn-k8s
---
apiVersion: v1alpha1
kind: AgentConfig
metadata:
  name: ${CLUSTER_NAME}
rendezvousIP: ${RENDEZVOUS_IP}
hosts:
  - hostname: ${BM01_HOSTNAME}
    interfaces:
      - name: eno1
        macAddress: ${BM01_MAC}
      - name: eno2
        macAddress: ${BM01_MAC_2}
    networkConfig:
      interfaces:
        - name: bond0
          type: bond
          state: up
          ipv4:
            enabled: true
            address:
              - ip: ${BM01_IP}
                prefix-length: 24 
            dhcp: false
          link-aggregation:
            mode: active-backup 
            options:
              miimon: "150" 
            port:
              - eno1
              - eno2
      dns-resolver:
        config:
          server:
            - ${NAMESERVER}
      routes:
        config:
          - destination: 0.0.0.0/0
            next-hop-address: ${GATEWAY}
            next-hop-interface: bond0
            table-id: 254

  - hostname: ${BM02_HOSTNAME}
    interfaces:
      - name: eno1
        macAddress: ${BM02_MAC}
      - name: eno2
        macAddress: ${BM02_MAC_2}
    networkConfig:
      interfaces:
        - name: bond0
          type: bond
          state: up
          ipv4:
            enabled: true
            address:
              - ip: ${BM02_IP}
                prefix-length: 24 
            dhcp: false
          link-aggregation:
            mode: active-backup 
            options:
              miimon: "150" 
            port:
              - eno1
              - eno2
      dns-resolver:
        config:
          server:
            - ${NAMESERVER}
      routes:
        config:
          - destination: 0.0.0.0/0
            next-hop-address: ${GATEWAY}
            next-hop-interface: bond0
            table-id: 254   

  - hostname: ${BM03_HOSTNAME}
    interfaces:
      - name: eno1
        macAddress: ${BM03_MAC}
      - name: eno2
        macAddress: ${BM03_MAC_2}
    networkConfig:
      interfaces:
        - name: bond0
          type: bond
          state: up
          ipv4:
            enabled: true
            address:
              - ip: ${BM03_IP}
                prefix-length: 24 
            dhcp: false
          link-aggregation:
            mode: active-backup 
            options:
              miimon: "150" 
            port:
              - eno1
              - eno2
      dns-resolver:
        config:
          server:
            - ${NAMESERVER}
      routes:
        config:
          - destination: 0.0.0.0/0
            next-hop-address: ${GATEWAY}
            next-hop-interface: bond0
            table-id: 254   