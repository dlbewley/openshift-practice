apiVersion: v1alpha1
kind: AgentConfig
metadata:
  name: ${CLUSTER_NAME}
rendezvousIP: ${RENDEZVOUS_IP}
hosts:
  - hostname: ${BM01_HOSTNAME}
    interfaces:
      - name: eno1
        macAddress: ${BM01_MAC}
    networkConfig:
      interfaces:
        - name: eno1
          type: ethernet
          state: up
          mac-address: ${BM01_MAC}
          ipv4:
            enabled: true
            address:
              - ip: ${BM01_IP}
                prefix-length: 24 
            dhcp: false
      dns-resolver:
        config:
          server:
            - ${NAMESERVER}
      routes:
        config:
          - destination: 0.0.0.0/0
            next-hop-address: ${GATEWAY}
            next-hop-interface: eno1
            table-id: 254

  - hostname: ${BM02_HOSTNAME}
    interfaces:
      - name: eno1
        macAddress: ${BM02_MAC}
    networkConfig:
      interfaces:
        - name: eno1
          type: ethernet
          state: up
          mac-address: ${BM02_MAC}
          ipv4:
            enabled: true
            address:
              - ip: ${BM02_IP}
                prefix-length: 24 
            dhcp: false
      dns-resolver:
        config:
          server:
            - ${NAMESERVER}
      routes:
        config:
          - destination: 0.0.0.0/0
            next-hop-address: ${GATEWAY}
            next-hop-interface: eno1
            table-id: 254

  - hostname: ${BM03_HOSTNAME}
    interfaces:
      - name: eno1
        macAddress: ${BM03_MAC}
    networkConfig:
      interfaces:
        - name: eno1
          type: ethernet
          state: up
          mac-address: ${BM03_MAC}
          ipv4:
            enabled: true
            address:
              - ip: ${BM03_IP}
                prefix-length: 24 
            dhcp: false
      dns-resolver:
        config:
          server:
            - ${NAMESERVER}
      routes:
        config:
          - destination: 0.0.0.0/0
            next-hop-address: ${GATEWAY}
            next-hop-interface: eno1
            table-id: 254